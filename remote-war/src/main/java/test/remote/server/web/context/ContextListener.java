package test.remote.server.web.context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ContextListener implements ServletContextListener,
		ApplicationContextAware {

	private static final Log LOG = LogFactory.getLog(ContextListener.class);
	
	public ContextListener() {
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		LOG.info("contextDestroyed");
	}

	public void contextInitialized(ServletContextEvent arg0) {
		LOG.info("contextInitialized");
	}

	public void setApplicationContext(ApplicationContext ctx)
			throws BeansException {
		final StringBuilder sb = new StringBuilder();
		sb.append("Spring beans:\n");
		for (String n : ctx.getBeanDefinitionNames()) {
			sb.append("\t");
			sb.append(n);
			sb.append(" -> ");
			final Object b = ctx.getBean(n);
			sb.append(b);
			sb.append(" (");
			sb.append(b.getClass().getName());
			sb.append(")\n");
		}
		sb.append("end of Spring beans\n");
		LOG.info(sb.toString());
	}

}
