package test.remote.client.main;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import test.remote.server.service.RemoteFactory;

@Configurable
public class OpTypes {

	private static final Log LOG = LogFactory.getLog(OpTypes.class);
	
	private static final Class<?>[] NO_ARGS = new Class<?>[0];
	
	static {
		ACSingleton.getApplicationContext();
	}

	@Autowired
	@Qualifier("remoteFactory")
	RemoteFactory factory;
	// = ACSingleton.getApplicationContext().getBean(RemoteFactory.class);

	public OpTypes() {
	}

	public static void main(String[] args) throws Exception {
		final OpTypes dut = new OpTypes();
		dut.calculate();
	}
	
	private void calculate() throws Exception {
		final Object o = factory.getRemote("test.remote.server.domain.OpType");
		final Class<?> oclazz = o.getClass();
		LOG.info("public methods: " + Arrays.asList(oclazz.getMethods()));
		final Method findAllOpTypes = oclazz.getMethod("findAllOpTypes", NO_ARGS);
		LOG.info("findAllOpTypes: " + findAllOpTypes.invoke(null));
	}
}
