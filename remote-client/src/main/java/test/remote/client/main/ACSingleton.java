package test.remote.client.main;

import java.security.Permission;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ACSingleton {

	private static final Log LOG = LogFactory.getLog(ACSingleton.class);

	private static final String PREFIX = "file:/home/tpasch/compile/remote-test/remote-client/src/main/resources/META-INF/spring/";

	private static ACSingleton INSTANCE = new ACSingleton();

	private final ApplicationContext ctx = new FileSystemXmlApplicationContext(
			PREFIX + "applicationContext.xml", PREFIX + "remoteBeans.xml");

	private ACSingleton() {
		// Allow RMI class loading - see
		// http://download.oracle.com/javase/tutorial/rmi/implementing.html
		//
		// TODO:
		// This is unacceptable in real life!
		//
		if (System.getSecurityManager() == null) {
			final SecurityManager sm = new SecurityManager() {
				@Override
				public void checkPermission(Permission p) {	
					LOG.info("checkPermission " + p);
				}
				@Override
				public void checkPermission(Permission p, Object o) {					
					LOG.info("checkPermission " + p + " on " + o);
				}
			};
			System.setSecurityManager(sm);
		}

		final StringBuilder sb = new StringBuilder();
		sb.append("Spring beans:\n");
		for (String n : ctx.getBeanDefinitionNames()) {
			sb.append("\t");
			sb.append(n);
			sb.append(" -> ");
			final Object b = ctx.getBean(n);
			sb.append(b);
			sb.append(" (");
			sb.append(b.getClass().getName());
			sb.append(")\n");
		}
		sb.append("end of Spring beans\n");
		LOG.info(sb.toString());
	}

	public static ApplicationContext getApplicationContext() {
		return INSTANCE.ctx;
	}

}
