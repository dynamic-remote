package test.remote.client.main;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import test.remote.server.service.Calculator;
import test.remote.server.service.RemoteFactory;

@Configurable
public class Calculate {

	private static final Log LOG = LogFactory.getLog(Calculate.class);
	
	static {
		ACSingleton.getApplicationContext();
	}

	private RemoteFactory factory;
	// = ACSingleton.getApplicationContext().getBean(RemoteFactory.class);

	public Calculate() {
	}

	public static void main(String[] args) throws Exception {
		final Calculate dut = new Calculate();
		dut.calculate();
	}
	
	private void calculate() throws Exception {
		final Calculator cal = factory.getRemote("test.remote.server.service.Calculator");
		Double result;
		try {
			cal.push("3");
			cal.push("4");
			cal.push("+");
			cal.push("5");
			cal.push("*");

			result = cal.eval();
			System.out.println("result is " + result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cal.clear();
		}
	}
	
	/**
	 * Needed because of the security manager.
	 */
	@Autowired
	@Qualifier("remoteFactory")
	public void setRemoteFactory(RemoteFactory remoteFactory) {
		this.factory = remoteFactory;
	}
 }
