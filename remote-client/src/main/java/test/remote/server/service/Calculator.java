package test.remote.server.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Calculator extends Remote {
	
	void push(String atom) throws RemoteException;
	
	Double eval() throws RemoteException;
	
	void clear() throws RemoteException;

}
