package test.remote.server.service;

import java.rmi.RemoteException;

public interface RemoteFactory {

	<T> T getRemote(String clazz, Object... hints) throws RemoteException;

}
