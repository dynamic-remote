package test.remote.server.domain;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.test.RooIntegrationTest;

@RooIntegrationTest(entity = Dummy.class)
public class MyDummyIntegrationTest {
	
	@Autowired
	private DummyDataOnDemand dod = new DummyDataOnDemand();

    @Test
    public void testMarkerMethod() {
    	Dummy d12 = dod.getNewTransientDummy(12);
    	System.out.println("d12: " + d12.toString());
    	d12.setDummy("d12");
    	d12.persist();
    	System.out.println("d12: " + d12.toString());
    }
}
