// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package test.remote.server.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import test.remote.server.domain.DummyDataOnDemand;

privileged aspect DummyIntegrationTest_Roo_IntegrationTest {
    
    declare @type: DummyIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: DummyIntegrationTest: @ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext.xml");
    
    declare @type: DummyIntegrationTest: @Transactional;
    
    @Autowired
    private DummyDataOnDemand DummyIntegrationTest.dod;
    
    @Test
    public void DummyIntegrationTest.testCountDummys() {
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", dod.getRandomDummy());
        long count = test.remote.server.domain.Dummy.countDummys();
        org.junit.Assert.assertTrue("Counter for 'Dummy' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void DummyIntegrationTest.testFindDummy() {
        test.remote.server.domain.Dummy obj = dod.getRandomDummy();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to provide an identifier", id);
        obj = test.remote.server.domain.Dummy.findDummy(id);
        org.junit.Assert.assertNotNull("Find method for 'Dummy' illegally returned null for id '" + id + "'", obj);
        org.junit.Assert.assertEquals("Find method for 'Dummy' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void DummyIntegrationTest.testFindAllDummys() {
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", dod.getRandomDummy());
        long count = test.remote.server.domain.Dummy.countDummys();
        org.junit.Assert.assertTrue("Too expensive to perform a find all test for 'Dummy', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        java.util.List<test.remote.server.domain.Dummy> result = test.remote.server.domain.Dummy.findAllDummys();
        org.junit.Assert.assertNotNull("Find all method for 'Dummy' illegally returned null", result);
        org.junit.Assert.assertTrue("Find all method for 'Dummy' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void DummyIntegrationTest.testFindDummyEntries() {
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", dod.getRandomDummy());
        long count = test.remote.server.domain.Dummy.countDummys();
        if (count > 20) count = 20;
        java.util.List<test.remote.server.domain.Dummy> result = test.remote.server.domain.Dummy.findDummyEntries(0, (int) count);
        org.junit.Assert.assertNotNull("Find entries method for 'Dummy' illegally returned null", result);
        org.junit.Assert.assertEquals("Find entries method for 'Dummy' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void DummyIntegrationTest.testFlush() {
        test.remote.server.domain.Dummy obj = dod.getRandomDummy();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to provide an identifier", id);
        obj = test.remote.server.domain.Dummy.findDummy(id);
        org.junit.Assert.assertNotNull("Find method for 'Dummy' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyDummy(obj);
        java.lang.Integer currentVersion = obj.getVersion();
        obj.flush();
        org.junit.Assert.assertTrue("Version for 'Dummy' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void DummyIntegrationTest.testMerge() {
        test.remote.server.domain.Dummy obj = dod.getRandomDummy();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to provide an identifier", id);
        obj = test.remote.server.domain.Dummy.findDummy(id);
        boolean modified =  dod.modifyDummy(obj);
        java.lang.Integer currentVersion = obj.getVersion();
        test.remote.server.domain.Dummy merged = (test.remote.server.domain.Dummy) obj.merge();
        obj.flush();
        org.junit.Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        org.junit.Assert.assertTrue("Version for 'Dummy' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void DummyIntegrationTest.testPersist() {
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", dod.getRandomDummy());
        test.remote.server.domain.Dummy obj = dod.getNewTransientDummy(Integer.MAX_VALUE);
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to provide a new transient entity", obj);
        org.junit.Assert.assertNull("Expected 'Dummy' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        org.junit.Assert.assertNotNull("Expected 'Dummy' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void DummyIntegrationTest.testRemove() {
        test.remote.server.domain.Dummy obj = dod.getRandomDummy();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Dummy' failed to provide an identifier", id);
        obj = test.remote.server.domain.Dummy.findDummy(id);
        obj.remove();
        obj.flush();
        org.junit.Assert.assertNull("Failed to remove 'Dummy' with identifier '" + id + "'", test.remote.server.domain.Dummy.findDummy(id));
    }
    
}
