package test.remote.server.main;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import test.remote.server.service.Calculator;
import test.remote.server.service.RemoteFactory;
import test.remote.server.service.impl.RemoteFactoryImpl;

public class Calculate {

	private static final Log LOG = LogFactory.getLog(Calculate.class);

	static {
		ACSingleton.getApplicationContext();
	}

	public Calculate() {
	}

	public static void main(String[] args) throws Exception {
		final RemoteFactory fac = new RemoteFactoryImpl();
		final Calculator cal = fac.getRemote("test.remote.server.service.Calculator");
		Double result;
		try {
			cal.push("3");
			cal.push("4");
			cal.push("+");
			cal.push("5");
			cal.push("*");

			result = cal.eval();
			System.out.println("result is " + result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cal.clear();
		}
	}
}
