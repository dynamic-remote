package test.remote.server.main;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ACSingleton {

	private static final Log LOG = LogFactory.getLog(ACSingleton.class);
	
	private static final String PREFIX = "file:/home/tpasch/compile/remote-test/remote-server/src/main/resources/META-INF/spring/";
	
	private static ACSingleton INSTANCE = new ACSingleton();

	private final ApplicationContext ctx = new FileSystemXmlApplicationContext(
			PREFIX + "applicationContext.xml", PREFIX + "remoteBeans.xml");

	private ACSingleton() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Spring beans:\n");
		for (String n: ctx.getBeanDefinitionNames()) {
			sb.append("\t");
			sb.append(n);
			sb.append(" -> ");
			final Object b = ctx.getBean(n);
			sb.append(b);
			sb.append(" (");
			sb.append(b.getClass().getName());
			sb.append(")\n");
		}
		sb.append("end of Spring beans\n");
		LOG.info(sb.toString());
	}

	public static ApplicationContext getApplicationContext() {
		return INSTANCE.ctx;
	}

}
