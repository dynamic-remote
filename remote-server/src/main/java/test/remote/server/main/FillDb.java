package test.remote.server.main;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import test.remote.server.domain.OpType;

public class FillDb {

	private static final Log LOG = LogFactory.getLog(FillDb.class);
	
	static {
		ACSingleton.getApplicationContext();
	}

	public FillDb() {
	}

	public static void main(String[] args) {
		List<OpType> types = OpType.findAllOpTypes();
		LOG.info("OpTypes: " + types);
		//
		OpType t = new OpType();
		try {
			t.setName("CONST");
			if (!types.contains(t))
				t.persist();
			//
			t = new OpType();
			t.setName("+");
			if (!types.contains(t))
				t.persist();
			//
			t = new OpType();
			t.setName("-");
			if (!types.contains(t))
				t.persist();
			//
			t = new OpType();
			t.setName("*");
			if (!types.contains(t))
				t.persist();
			//
			types = OpType.findAllOpTypes();
			LOG.info("OpTypes: " + types);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("failed on " + t);
			System.err.println("\tcontains " + types.contains(t));
			final OpType ttc = types.get(3);
			System.err.println("\tequals to " + ttc + ": " + t.equals(ttc));
		}
	}
}
