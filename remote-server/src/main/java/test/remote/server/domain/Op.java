package test.remote.server.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import test.remote.server.domain.OpType;
import test.remote.server.service.impl.Apply;

import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooEntity
public abstract class Op {
	
    // @NotNull
    @ManyToOne
    private OpType type;
    
    public abstract void visit(Apply apply);
    
}
