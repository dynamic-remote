package test.remote.server.domain;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;
import javax.validation.constraints.Size;
import de.saxsys.roo.equals.addon.RooEquals;

@RooJavaBean
@RooToString
@RooEntity
// @RooEquals
public class OpType implements Serializable {

    @NotNull
    @Column(unique = true)
    @Size(max = 10)
    private String name;

    public boolean equals(Object other) {
        if (other == null) { return false; }
        if (other == this) { return true; }
        if (other.getClass() != getClass()) {
        	return false;
        }
        OpType rhs = (OpType) other;
        return new EqualsBuilder()
            .append(name, rhs.name)
            .isEquals();
    }
    
    public int hashCode() {
        return new HashCodeBuilder(43, 11)
            .append(name)
            .toHashCode();
    }
    
}
