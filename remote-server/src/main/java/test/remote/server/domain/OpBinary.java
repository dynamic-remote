package test.remote.server.domain;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

import test.remote.server.service.impl.Apply;

import de.saxsys.roo.equals.addon.RooEquals;

@RooJavaBean
@RooToString
@RooEntity
@RooEquals(callSuper=true)
public class OpBinary extends Op {

    @NotNull
    @ManyToOne
    private Op left;
	
    @NotNull
    @ManyToOne
    private Op right;

	@Override
	public void visit(Apply apply) {
		final String memoric = getType().getName();
		if ("+".equals(memoric)) {
			apply.push(apply.pop() + apply.pop());
		}
		else if ("-".equals(memoric)) {
			apply.push(apply.pop() - apply.pop());
		}
		else if ("*".equals(memoric)) {
			apply.push(apply.pop() * apply.pop());
		}
		else {
			throw new IllegalStateException();
		}
	}
	
}
