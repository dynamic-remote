package test.remote.server.domain;

import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

import test.remote.server.service.impl.Apply;

import de.saxsys.roo.equals.addon.RooEquals;

@RooJavaBean
@RooToString
@RooEntity
@RooEquals(callSuper=true)
public class OpConstant extends Op {
	
	@NotNull
	private double value;

	@Override
	public void visit(Apply apply) {
		apply.push(value);
	}

}
