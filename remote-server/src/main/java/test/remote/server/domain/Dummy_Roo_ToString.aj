// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package test.remote.server.domain;

import java.lang.String;

privileged aspect Dummy_Roo_ToString {
    
    public String Dummy.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Dummy: ").append(getDummy()).append(", ");
        sb.append("Id: ").append(getId()).append(", ");
        sb.append("Version: ").append(getVersion());
        return sb.toString();
    }
    
}
