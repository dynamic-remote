package test.remote.server.service.impl;

public interface Apply {

	double pop();
	
	void push(double d);
	
}
