package test.remote.server.service.impl;

import java.util.ArrayList;
import java.util.List;


public class ApplyImpl implements Apply {
	
	private final List<Double> stack = new ArrayList<Double>();

	@Override
	public double pop() {
		return stack.remove(stack.size() - 1).doubleValue();
	}

	@Override
	public void push(double d) {
		stack.add(d);
	}

}
