package test.remote.server.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

import test.remote.server.domain.OpConstant;

public interface Calculator extends Remote {
	
	void push(String atom) throws RemoteException;
	
	/* *very* bad idea *
	OpConstant eval();
	*/
	Double eval() throws RemoteException;
	
	void clear() throws RemoteException;

}
