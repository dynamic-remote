package test.remote.server.service.impl;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import test.remote.server.service.RemoteFactory;

@Service
public class RemoteFactoryImpl implements RemoteFactory {

	private static final Log LOG = LogFactory.getLog(RemoteFactoryImpl.class);

	public RemoteFactoryImpl() {
	}

	@Override
	public <T> T getRemote(String clazz, Object... hints)
			throws RemoteException {
		try {
			Class<?> oclass = Thread.currentThread().getContextClassLoader()
					.loadClass(clazz);
			final Object result;
			if (oclass.isInterface()) {
				result = new CalculatorImpl();
			} else {
				result = oclass.newInstance();
			}
			LOG.info("getRemote(" + clazz + ") instantiated " + result);
			if (result instanceof Remote) {
				return (T) UnicastRemoteObject.exportObject((Remote) result, 0);
			} else {
				return (T) result;
			}
		} catch (ClassNotFoundException e) {
			throw new RemoteException(e.toString(), e);
		} catch (InstantiationException e) {
			throw new RemoteException(e.toString(), e);
		} catch (IllegalAccessException e) {
			throw new RemoteException(e.toString(), e);
		}
	}

}
