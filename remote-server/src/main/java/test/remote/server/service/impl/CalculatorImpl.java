package test.remote.server.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import test.remote.server.domain.Op;
import test.remote.server.domain.OpBinary;
import test.remote.server.domain.OpConstant;
import test.remote.server.domain.OpType;
import test.remote.server.service.Calculator;

@Service
public class CalculatorImpl implements Calculator {
	
	private List<Long> stack = new ArrayList<Long>();
	
	public CalculatorImpl() {
	}

	@Override
	public void push(String atom) {
		final Map<String,OpType> str2Type = new HashMap<String,OpType>();
		for (OpType t: OpType.findAllOpTypes()) {
			str2Type.put(t.getName(), t);
		}
		
		if (atom == null) {
			throw new NullPointerException();
		}
		if (atom.equals("+") || atom.equals("-") || atom.equals("*")) {
			final int size = stack.size();
			final OpBinary op = new OpBinary();
			op.setType(str2Type.get(atom));
			op.setLeft(Op.findOp(stack.get(size - 1)));
			op.setRight(Op.findOp(stack.get(size - 2)));
			op.persist();
			stack.add(op.getId());
		}
		else {
			Double v = Double.parseDouble(atom);
			final OpConstant op = new OpConstant();
			op.setType(str2Type.get("CONST"));
			op.setValue(v);
			op.persist();
			stack.add(op.getId());
		}
	}

	@Override
	public Double eval() {
		final Apply apply = new ApplyImpl();
		for (Long id: stack) {
			final Op op = Op.findOp(id);
			op.visit(apply);
		}
		final OpConstant result = new OpConstant();
		result.setValue(apply.pop());
		return result.getValue();
	}

	@Override
	public void clear() {
		for (Long l: stack) {
			Op.findOp(l).remove();
		}
	}
	
}
